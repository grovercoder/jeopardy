# TODO:

- finish styling admin
- convert participants to be more dynamic
    - currently it takes a page refresh to repaint new teams

- update test page to handle sockets
    - replace checkboxes with buttons
        - dynamic list based on team-refresh

    - make sure the form post can still be simulated
        - maybe a toggle to choose a form post vs socket communication





# Notes to myself for planning development approach

Admin Tasks
    - Manage Teams
        - Add / Remove
        - Set team avatar
    - Add points to team
    - Reset button status
    - map button colors to teams
    

Participant
    - push button
    - see which team was "first"
    - see scores for all teams

System
    - track team data
    - receive button press winners
    - display participants screen
    - display admin screen


System Objects:
    - teams
        - avatar image (URL to image)
        - current score
        - button color
    
