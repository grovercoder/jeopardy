const IDLE = 'idle'
const BUZZED = 'buzzed'

const teams = []
const newTeam = (data) => {
    return {
        key: Math.random().toString(36).substr(2, 9),
        name: data?.name,
        avatar: data?.avatar || '/img/avatar.png',
        score: parseInt(data?.score, 10) || 0,
        button: data?.button,
        button_state: IDLE,
        rgb: data?.rgb
    }
}

const defaultTeams = () => {
    teams.length = 0
    teams.push(newTeam({ name: "Red", avatar: null, button: 'red', rgb: "#f00" }))
    teams.push(newTeam({ name: "Green", avatar: null, button: 'green', rgb: "#0f0" }))
    teams.push(newTeam({ name: "Blue", avatar: null, button: 'blue', rgb: "#00f" }))
    teams.push(newTeam({ name: "White", avatar: null, button: 'white', rgb: "#fff" }))
}

// function set_state(message) {
//     if (message?.code) {
//         const buttons = message.code.toLowerCase().split(' ')
//         for (const btn of buttons) {
//             teams.forEach(team => {
//                 if (team.button == btn) {
//                     team.button_state = "buzzed"
//                 }
//             })
//         }
//     }
// }


// special case when we only know what button was pushed, but not the corresponding team
function buzz_button(code) {
    const team = teams.find(row => row.button == code)
    if (team) { team.button_state = BUZZED }
}

function setTeamState(key, desired) {
    console.log({ key, desired })
    let state = IDLE
    if (desired === true || desired == BUZZED) {
        state = BUZZED
    }

    for (const team of teams) {
        if (team.button.toLowerCase() == key.toLowerCase()) {
            team.button_state = state
        }
    }
}


function reset_buttons() {
    teams.forEach(team => {
        team.button_state = IDLE
    })
}

function find(key) {
    return teams.find(team => team.key == key)
}

function save(data) {
    if (!data) return;

    let team = find(data.key)
    if (!team) {
        teams.push(newTeam(data))
    }
    else {
        Object.keys(team).forEach(prop => {
            if (data[prop]) {
                team[prop] = data[prop]
            }
        })
    }
}

function reset_scores() {
    for (const team of teams) {
        team.score = 0
    }
}

function reset_scores() {
    for (const team of teams) {
        team.score = 0
        team.button_state = IDLE
    }
}

function award(teamKey, amount) {
    const team = teams.find(row => row.key == teamKey)
    if (team) {
        team.score += (parseInt(amount, 10) || 0)
    }
}

function buzz_in(teamKey) {
    const team = find(teamKey)

    if (team) { team.button_state = BUZZED }
}

function buzz_idle(teamKey) {
    const team = find(teamKey)

    if (team) { team.button_state = IDLE }
}

function set_score(data) {
    const team = find(data.team)
    if (team) {
        team.score = parseInt(data.score, 10) || 0
    }
}

defaultTeams()

module.exports = {
    award,
    buzz_button,
    buzz_in,
    buzz_idle,
    clear: () => { teams.length = 0 },
    get: () => teams,
    reset_buttons,
    reset_scores,
    setTeamState,
    set_score,
    save
}