const path = require('path')
const Express = require('express')
const cors = require('cors')
const app = Express()
const handlebars = require('express-handlebars')
const expressWs = require('express-ws')
const Teams = require('./teams.js')
const teams = require('./teams.js')
const clients = require('./socket.js')
const Config = require('../config.js')

// Setup the Handlebars template engine
const viewpath = path.resolve(__dirname, 'views')

app.engine('hbs', handlebars.engine({
    layoutsDir: viewpath,
    defaultLayout: 'main',
    extname: '.hbs'
}))

app.set('views', viewpath)
app.set('view engine', 'hbs')

// setup parsing of JSON and form data
app.use(Express.urlencoded({ extended: true }))
app.use(Express.json())

// Static files
app.use(Express.static(path.resolve(__dirname, './public')))


// setup cors
app.use(cors())

// setup the socket server
expressWs(app)

// set up simple routes
app.get('/', (req, res) => {
    const context = {
        page: 'participants'
    }

    res.render('participants', context)
})

app.get('/admin', (req, res) => {
    res.render('admin', { page: 'admin' })
})

app.get('/test', (req, res) => {
    const teams = Teams.get()
    const context = {
        red: teams.find(team => team.button == 'red').button_state == 'buzzed',
        blue: teams.find(team => team.button == 'blue').button_state == 'buzzed',
        green: teams.find(team => team.button == 'green').button_state == 'buzzed',
        white: teams.find(team => team.button == 'white').button_state == 'buzzed',
    }

    res.render('test', context)
})

app.post('/buzz', (req, res) => {
    if (Array.isArray(req.body.code)) {
        for (btn of req.body.code) {
            teams.buzz_button(btn)
        }
    }
    else {
        if (req.body) {
            for (const team of teams.get()) {
                teams.setTeamState(team.button, (req.body[team.button]) ? 'buzzed' : 'idle')
            }
            // The loop above effectively does the same as many of these lines
            // teams.setTeamState('red', (req.body?.red) ? 'buzzed' : 'idle')
        }
    }

    clients.refreshTeams()

    res.sendStatus(200)
    // res.sendStatus(200)
})

app.ws('/socket', (ws, req) => {
    const client = clients.add(ws)

    client.on('message', event => {
        const response = clients.handleSocketEvent(event)
        if (response) {
            ws.send(JSON.stringify(response))
        }
    })

    client.on('close', () => {
        console.log('client socket was closed')
    })

})

app.listen(Config.web_port, () => {
    console.log(`Listening on port # ${Config.web_port}`)
})
