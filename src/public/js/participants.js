
const protocol = (window.location.protocol === 'https') ? 'wss:' : 'ws:'
const apiSocket = protocol + '//' + window.location.hostname + ':' + window.location.port + '/socket'
const socket = new WebSocket(apiSocket)

socket.onopen = () => {
    socket.send(JSON.stringify({ type: 'query-teams' }))
}

socket.onmessage = event => {
    const data = JSON.parse(event?.data)
    if (data.type == 'team-refresh') {
        updateTeams(data.payload)
    }
}

function updateTeams(data) {
    if (!data) return

    const container = document.querySelector('.team-list')
    container.innerText = ''

    if (Array.isArray(data)) {
        data.forEach(team => {
            container.appendChild(teamContainer(team))
        })
    }
}

function teamContainer(team) {
    const divteam = document.createElement("div")
    const divteamname = document.createElement("div")
    const divavatar = document.createElement("div")
    const imgavatar = document.createElement("img")
    const divbutton = document.createElement("div")
    const divscore = document.createElement("div")

    divteam.id = `team-${team.key}`
    divteam.className = "team"

    divteamname.className = "teamname"
    divteamname.innerText = team.name

    divavatar.className = "avatar"
    imgavatar.src = (team.avatar) ? team.avatar : "/img/avatar.png"
    imgavatar.alt = 'team avatar'
    divavatar.appendChild(imgavatar)

    divbutton.className = "button-indicator"
    divbutton.classList.add(team.button_state)

    divscore.className = "score"
    divscore.innerText = (parseInt(team.score)) ? parseInt(team.score, 10) : 0

    divteam.appendChild(divteamname)
    divteam.appendChild(divavatar)
    divteam.appendChild(divbutton)
    divteam.appendChild(divscore)

    divbutton.addEventListener('click', event => {
        event.stopPropagation()
        buzzIn(team.key)
    })

    return divteam
}

function buzzIn(teamKey) {
    socket.send(JSON.stringify({ type: 'cmd-buzzed', payload: teamKey }))
}