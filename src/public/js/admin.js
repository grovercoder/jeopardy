const protocol = (window.location.protocol === 'https') ? 'wss:' : 'ws:'
const apiSocket = protocol + '//' + window.location.hostname + ':' + window.location.port + '/socket'

const socket = new WebSocket(apiSocket)

let TEAMS = []

socket.onopen = () => {
    socket.send(bundleEvent('query', 'admin-ui'))
}

socket.onmessage = event => {
    handleSocketMessage(event)
}

const defaultTeam = {
    key: "",
    name: "",
    avatar: "",
    score: "",
    button: "",
    button_state: "",
    rgb: ""
}

const dialogControls = {
    dialog: document.getElementById('teamDialog'),
    key: document.getElementById('inputTeamKey'),
    name: document.getElementById('inputTeamName'),
    avatar: document.getElementById('inputTeamAvatar'),
    score: document.getElementById('inputTeamScore'),
    button: document.getElementById('inputTeamButton'),
}

function bundleEvent(tag, payload) {
    return JSON.stringify({
        type: tag,
        payload
    })
}

function handleSocketMessage(event) {
    let msg = {}

    try {
        msg = JSON.parse(event?.data)
    }
    catch (err) {
        // invalid JSON string, ignore
        console.log('invalid json', event)
        return
    }

    if (msg.type == 'team-refresh') {
        TEAMS = msg.payload
        updateUI()
    }
}

function updateUI() {

    const container = document.querySelector('#admin .team-list')
    container.innerHTML = ''

    for (const team of TEAMS) {
        const divteam = document.createElement('div')
        const divavatar = document.createElement('div')
        const divname = document.createElement('div')
        const divscore = document.createElement('div')
        const divcontrols = document.createElement('div')
        const inputScore = document.createElement('input')
        const divbuttonstate = document.createElement('div')
        const divBSText = document.createElement('div')
        const btnBSClear = document.createElement('button')

        divteam.classList.add('team')
        divavatar.classList.add('avatar')
        divname.classList.add('teamname')
        divscore.classList.add('score')
        divcontrols.classList.add('controls')

        if (team.button_state == 'buzzed') {
            divteam.classList.add('buzzed')
        }

        if (team.avatar) {
            const img = document.createElement('img')
            img.src = team.avatar
            divavatar.appendChild(img)
        }
        divname.innerText = team.name

        inputScore.type = "number"

        inputScore.value = team.score
        divscore.appendChild(inputScore)

        // controls
        const btn100 = document.createElement('button')
        const btn200 = document.createElement('button')
        const btn400 = document.createElement('button')
        const btn800 = document.createElement('button')

        btn100.innerText = '100'
        btn200.innerText = '200'
        btn400.innerText = '400'
        btn800.innerText = '800'
        divcontrols.appendChild(btn100)
        divcontrols.appendChild(btn200)
        divcontrols.appendChild(btn400)
        divcontrols.appendChild(btn800)

        divbuttonstate.className = "buttonstate"
        divBSText.className = "state"
        divBSText.innerText = team.button_state
        btnBSClear.innerText = 'Set Idle'
        divbuttonstate.appendChild(divBSText)
        divbuttonstate.appendChild(btnBSClear)


        divteam.appendChild(divavatar)
        divteam.appendChild(divname)
        divteam.appendChild(divcontrols)
        divteam.appendChild(divbuttonstate)
        divteam.appendChild(divscore)
        container.appendChild(divteam)

        divavatar.addEventListener('click', (event) => {
            populateTeamDialog(team)
            dialogControls.dialog.showModal()
        })

        btn100.addEventListener('click', event => {
            event.preventDefault()
            socket.send(bundleEvent('cmd-award', { team: team.key, amount: 100 }))
        })
        btn200.addEventListener('click', event => {
            event.preventDefault()
            socket.send(bundleEvent('cmd-award', { team: team.key, amount: 200 }))
        })
        btn400.addEventListener('click', event => {
            event.preventDefault()
            socket.send(bundleEvent('cmd-award', { team: team.key, amount: 400 }))
        })
        btn800.addEventListener('click', event => {
            event.preventDefault()
            socket.send(bundleEvent('cmd-award', { team: team.key, amount: 800 }))
        })

        inputScore.addEventListener('change', event => {
            console.log({
                team: team.key,
                score: inputScore.value
            })
            socket.send(bundleEvent('cmd-set-score', { team: team.key, score: parseInt(inputScore.value, 10) || 0 }))
        })

        divBSText.addEventListener('click', event => {
            socket.send(bundleEvent('cmd-buzzed', team.key))
        })
        btnBSClear.addEventListener('click', event => {
            socket.send(bundleEvent('cmd-idle', team.key))
        })
    }
}


function populateTeamDialog(team) {
    dialogControls.key.value = team.key
    dialogControls.name.value = team.name
    dialogControls.avatar.value = team.avatar
    dialogControls.score.value = team.score
    dialogControls.button.value = team.button
}

function showNewTeam() {
    populateTeamDialog({ ...defaultTeam })
    dialogControls.dialog.showModal()
}


function findTeam(key) {
    if (Array.isArray(TEAMS)) {
        return TEAMS.find(team => team.key == key)
    }
    return null
}

function saveTeam() {

    const team = findTeam(dialogControls.key.value) || {}

    team.key = dialogControls.key.value
    team.name = dialogControls.name.value
    team.avatar = dialogControls.avatar.value
    team.score = dialogControls.score.value
    team.button = dialogControls.button.value

    socket.send(bundleEvent('cmd-team-save', team))
}


function domEventHandlers() {
    const btnNewTeam = document.getElementById('btnNewTeam')
    const btnTeamDialogCancel = document.getElementById('btnTeamDialogCancel')
    const btnTeamDialogSave = document.getElementById('btnTeamDialogSave')
    const btnClearTeams = document.getElementById('btnClearTeams')
    const btnResetScores = document.getElementById('btnResetScores')
    const btnClearStates = document.getElementById('btnClearStates')

    btnTeamDialogCancel.addEventListener('click', event => {
        event.preventDefault()
        dialogControls.dialog.close()
    })

    btnTeamDialogSave.addEventListener('click', event => {
        event.preventDefault()
        saveTeam()
        dialogControls.dialog.close()
    })

    btnNewTeam.addEventListener('click', event => {
        event.preventDefault()
        showNewTeam()
    })

    btnClearTeams.addEventListener('click', event => {
        event.preventDefault()
        if (confirm('Remove all teams?')) {
            socket.send(bundleEvent('cmd-clear-teams'))
        }
    })

    btnResetScores.addEventListener('click', event => {
        event.preventDefault()
        if (confirm('Reset all team scores to zero?')) {
            socket.send(bundleEvent('cmd-reset-scores'))
        }
    })

    btnClearStates.addEventListener('click', event => {
        event.preventDefault()
        socket.send(bundleEvent('cmd-reset-buttons'))
    })

}

// register event handlers when the document is ready
document.addEventListener('DOMContentLoaded', (event) => {
    domEventHandlers()
})