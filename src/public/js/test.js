const protocol = (window.location.protocol === 'https') ? 'wss:' : 'ws:'
const apiSocket = protocol + '//' + window.location.hostname + ':' + window.location.port + '/socket'
const socket = new WebSocket(apiSocket)

const testConfig = {
    method: 'socket'
}

socket.onopen = () => {
    socket.send(JSON.stringify({ type: 'query-teams' }))
}

socket.onmessage = event => {
    const data = JSON.parse(event?.data)
    if (data.type == 'team-refresh') {
        updateTeams(data.payload)
    }
}

function updateTeams(teams) {
    const container = document.querySelector('#test .buttons')
    container.innerText = ''

    for (const team of teams) {
        const button = document.createElement('div')
        button.innerText = team.name
        button.className = 'btn'
        button.classList.add(team.button_state)

        button.addEventListener('click', event => {
            event.stopPropagation()
            sendBuzz(team)
        })

        container.appendChild(button)
    }
}

function sendBuzz(team) {
    if (testConfig.method == 'socket') {
        socket.send(JSON.stringify({ type: 'cmd-buzzed', payload: team.key }))
    }

    if (testConfig.method == 'form') {
        console.log({ team })
        const body = {
            code: [team.button]
        }
        console.log({ body })
        const options = {
            method: 'POST',
            mode: 'cors',
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded'
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }

        fetch('/buzz', options)
    }
}

function domEventHandlers() {
    const btnSocket = document.querySelector('.btn.socket')
    const btnForm = document.querySelector('.btn.form')
    const btnClear = document.querySelector('.btn.clear')

    btnSocket.addEventListener('click', event => {
        event.stopPropagation()
        testConfig.method = 'socket'
        btnSocket.classList.add('on')
        btnForm.classList.remove('on')
    })

    btnForm.addEventListener('click', event => {
        event.stopPropagation()
        testConfig.method = 'form'
        btnForm.classList.add('on')
        btnSocket.classList.remove('on')
    })

    btnClear.addEventListener('click', event => {
        event.stopPropagation()
        socket.send(JSON.stringify({ type: 'cmd-reset-buttons' }))
    })
}


document.addEventListener('DOMContentLoaded', event => {
    domEventHandlers()
})