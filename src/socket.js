const Teams = require('./teams.js')
const clients = []

function add(socket) {
    clients.push(socket)
    return socket
}

function sendAll(tag, payload) {
    const msg = bundleEvent(tag, payload)
    for (const socket of clients) {
        socket.send(JSON.stringify(msg))
    }
}

function refreshTeams() {
    const msg = bundleEvent('team-refresh', Teams.get())
    for (const socket of clients) {
        socket.send(JSON.stringify(msg))
    }
}

function bundleEvent(tag, payload) {
    return {
        type: tag,
        payload
    }
}

function handleSocketEvent(event) {
    if (!event) return
    let msg = {}
    try {
        msg = JSON.parse(event)
    } catch (err) {
        return
    }
    console.log({ msg })
    if (msg.type == 'cmd-award') { Teams.award(msg.payload.team, msg.payload.amount) }
    if (msg.type == 'cmd-buzzed') { Teams.buzz_in(msg.payload) }
    if (msg.type == 'cmd-idle') { Teams.buzz_idle(msg.payload) }
    if (msg.type == 'cmd-clear-teams') { Teams.clear() }
    if (msg.type == 'cmd-reset-buttons') { Teams.reset_buttons() }
    if (msg.type == 'cmd-reset-scores') { Teams.reset_scores() }
    if (msg.type == 'cmd-set-score') { console.log({ setScore: msg.payload }); Teams.set_score(msg.payload) }
    if (msg.type == 'cmd-team-save') { Teams.save(msg.payload) }
    if (msg.type == 'query-teams') { return bundleEvent('team-refresh', Teams.get()) }

    refreshTeams()
}


module.exports = {
    add,
    sendAll,
    handleSocketEvent,
    refreshTeams
}